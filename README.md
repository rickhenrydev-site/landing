# My Website (In Progress)

My website, currently under construction, that will be for advertising and used as a client portal.`

## Public Area (this repo)

* Tells a bit about me, the skills I offer, shares some demos.
* Contact form for inquiries and estimates.
* Good Call to action.

## Client Area

* Passwordless authentication with codes and/or links.
* Shows client info (name, email, company).
* Invoices, outstanding balance
* list of projects

### Projects

* Project name
* Project description
* Links to any deployments
* See latest commit message(s) with link
* See latest release with description, download link, and link to more
* Link to actual code on gitlab
* Link to any associated documentation
* Links to any specifications, collaborative documents on google docs, etc.
* direct link to issue/ticket
* Links/descriptions to associated resources
* (FUTURE): analytics / links
